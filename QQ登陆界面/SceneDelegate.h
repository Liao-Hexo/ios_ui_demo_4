//
//  SceneDelegate.h
//  QQ登陆界面
//
//  Created by 廖家龙 on 2020/4/20.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

