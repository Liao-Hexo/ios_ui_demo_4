//
//  ViewController.m
//  QQ登陆界面
//
//  Created by 廖家龙 on 2020/4/20.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

- (IBAction)login;//登陆方法
@property (weak, nonatomic) IBOutlet UITextField *textQQ;//QQ属性
@property (weak, nonatomic) IBOutlet UITextField *textpassword;//密码属性

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//登陆方法的实现
- (IBAction)login {
    
    //拿到QQ和密码
    NSString *qq=self.textQQ.text;
    NSString *pwd=self.textpassword.text;
    
    //将QQ和密码输出
    NSLog(@"QQ:%@,密码:%@",qq,pwd);
    
    //收回键盘
    [self.view endEditing:YES];
}
@end
